Rails.application.routes.draw do
  #devise_for :admins
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  # root 'application#application'
  
      #resources :customers 
      #resources :products
      #get 'customers/index'
      #get 'products/index'
      #get 'admins/index'
  #root 'admins#welcome'
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
   
  devise_scope :admin do
    authenticated :admin do
      #get 'sign_in', to: 'devise/layouts#application'
      # get "/sign_in", :to => "devise/sessions#create" # Add a custom sign in route for user sign in
      #get "/logout", :to => "devise/sessions#destroy" # Add a custom sing out route for user sign out
      #get "/register", :to => "devise/registrations#new" # Add a Custom Route for Registrations
      resources :customers do
        member do
          put 'switch', to: 'customers#status_switch'
          get 'edit', to: 'customers#edit'
        end
      end
      
      resources :products
      resources :orders
      
      root 'admins#index', as: :authenticated_admins_root
      #root 'admins#index'
    end
     
  end
  
  devise_for :admins
  root 'admins#welcome'
end