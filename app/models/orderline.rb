class Orderline < ActiveRecord::Base
  include ActiveModel::Dirty
  
  belongs_to :product
  belongs_to :order
  
  after_save :set_is_discounted
  after_save :set_total_amount
  
  before_create :update_product_qty_on_create
  before_update :update_product_qty_on_update
  before_destroy :update_product_qty_on_destroy
  
  def discount_amount
    base_amt = (self.product.base_price * self.quantity_in_kg)
    discounted_amt = base_amt - (base_amt * ((self.discount_rate || 0) / 100))
    discounted_amt
  end
  
  def undiscounted_total_amount
    self.product.base_price * self.quantity_in_kg
  end
  
  private
    def set_is_discounted
      self.update_columns(is_discounted: !self.discount_rate.blank?)
    end
    def set_total_amount
      base_amt = (self.product.base_price * self.quantity_in_kg)
      discounted_amt = base_amt - (base_amt * ((self.discount_rate || 0) / 100))
      self.update_columns(total_amount: discounted_amt)
      
      total_price = 0
      self.order.orderlines.each do |orderline|
        total_price += orderline.total_amount || 0
      end
      self.order.update_columns(total_price: total_price)
    end
    
    def update_product_qty_on_create
      new_qty = self.product.quantity - self.quantity_in_kg
      self.product.update_columns(quantity: new_qty)
    end
    def update_product_qty_on_update
      prev_val = self.quantity_in_kg_was
      product_qty_before_this = self.product.quantity + prev_val
      new_qty = product_qty_before_this - self.quantity_in_kg
      self.product.update_columns(quantity: new_qty)
    end
    def update_product_qty_on_destroy
      new_qty = self.product.quantity + self.quantity_in_kg
      self.product.update_columns(quantity: new_qty)
    end
end