class Order < ActiveRecord::Base
    belongs_to :customer
    
    has_many :order_lines, inverse_of: :order, dependent: :delete_all
    has_many :products, through: :order_lines
    accepts_nested_attributes_for :order_lines, allow_destroy: true, reject_if: :reject_order_lines
    
    has_many :payments, inverse_of: :order, dependent: :delete_all
    has_many :customers, through: :payments
    accepts_nested_attributes_for :payments, allow_destroy: true, reject_if: :reject_payments
    
    private
        def reject_order_lines
            false
        end
        def reject_payments
            false
        end
end
