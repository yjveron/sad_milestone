class Customer < ActiveRecord::Base
    has_many :orders
    has_many :payments
    
    def toggle(column)
        self.send(:status=, !self.public_send(:status))
        self.save
    end
end
