class Payment < ActiveRecord::Base
    belongs_to :order
    belongs_to :customer
    
    before_save :set_customer_id
    
    private
        def set_customer_id
           self.customer_id = self.order.customer_id 
        end
end
