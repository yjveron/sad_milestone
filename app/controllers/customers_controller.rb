class CustomersController < ApplicationController
    include SmartListing::Helper::ControllerExtensions
    helper  SmartListing::Helper
    
    before_filter :find_customer, except: [:index, :new, :create]

    def index
      smart_listing_create(:customers, 
        Customer.all, 
        partial: "customers/list",
        default_sort: {name: "asc"})
        
      respond_to do |format|
        format.html
        format.js
      end
    end
    
    def new
      @customer = Customer.new
    end
    
    def create
      @customer = Customer.create(customer_params)
    end
    
    def edit
    end
    
    def update
      if @customer.update(customer_params)
         redirect_to customers_path
        else
          render :edit
      end
    end
    
    def destroy
      @user.destroy
    end
    
    def status_switch
      @customer.toggle(:status)
    end
    
    def is_loyal_switch
      @customer.toggle(:is_loyal)
    end
    
    private
    
    def find_customer
      @customer = Customer.find(params[:id])
    end
    
    def customer_params
      params.require(:customer).permit(:name, :company, :phone, 
                                   :email, :home_address, :status, :is_loyal )
                                   #or include :session_id    ?
    end
    
end
