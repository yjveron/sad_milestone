class ProductsController < ApplicationController
    include SmartListing::Helper::ControllerExtensions
    helper  SmartListing::Helper
    
    before_filter :find_product, except: [:index, :new, :create]

    def index
      @products = smart_listing_create(:products, 
        Product.all, 
        partial: "products/list",
        default_sort: {name: "asc"})
        
      respond_to do |format|
        format.html
        format.js
      end
    end
    
    def new
      @product = Product.new
    end
    
    def create
      @product = Product.create(product_params)
    end
    
    def edit
    end
    
    def update
      if @product.update(product_params)
         redirect_to products_path
        else
          render :edit
      end
    end
    
    def destroy
      @user.destroy
    end
    
    def status_switch
      @product.toggle(:status)
    end

    private
    def find_product
      @product = Product.find(params[:id])
    end
    
    def product_params
      params.require(:product).permit(:name, :threshold, :base_price,
                                    :quantity, :status)
                                   #or include :session_id    ?
    end
    
end
