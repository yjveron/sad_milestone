class OrdersController < ApplicationController
    include SmartListing::Helper::ControllerExtensions
    helper  SmartListing::Helper

    before_action :get_options_for_select, only: [:new, :edit]
    
    def index
        @selected_month = !params[:month].present? || params[:month] == 0 ? 0 : params[:month]
        @selected_year = !params[:year].present? || params[:year] == 0 ? 0 : params[:year]
        
        order_scope = Order.all
        if @selected_month != 0 && @selected_year != 0
            order_scope = Order.where(
                'EXTRACT(MONTH FROM ORDER_DATE) = ? AND EXTRACT(YEAR FROM ORDER_DATE) = ?',
                @selected_month,
                @selected_year
            )
        end
        order_scope.order(order_date: :desc)
        @orders = smart_listing_create :orders, order_scope, partial: 'list'
    end

    def new
        @order = Order.new
    end
    
    def create
       @order = Order.new order_params
       if @order.save
           redirect_to orders_path(@order)
       else
           render :new
       end
    end
    
    def show
        @order = Order.find params[:id]
    end
    
    def edit
        @order = Order.find params[:id]
    end
    
    def update
        @order = Order.find params[:id]
        if @order.update order_params
            redirect_to orders_path(@order)
        else
            render :edit
        end
    end
    
    def destroy
       @order = Order.find params[:id]
       @order.destroy
    end
    
    private
        def order_params
            params.require(:order).permit!
        end
        
        def get_options_for_select
            @customer_opts = Customer.pluck(:name, :company, :id).map { |arr| [ "#{arr[0]} (#{arr[1]})", arr[2] ] }
            @product_opts = Product.pluck(:name, :id)
            @order_status_opts = ['Received', 'Returned', 'Replacement', 'Unpaid', 'Partially paid', 'Paid']
            @order_line_status_opts = ['Received', 'Returned', 'Replacement']
            @payment_type_opts = ['Cash', 'Check', 'Credit']
        end
end
