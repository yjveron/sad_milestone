# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180517141006) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "name",                   limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "customers", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.string   "company",      limit: 255
    t.string   "phone",        limit: 255
    t.string   "email",        limit: 255
    t.string   "home_address", limit: 255
    t.boolean  "status"
    t.boolean  "is_loyal"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "order_lines", force: :cascade do |t|
    t.decimal  "quantity_of_product",             precision: 10
    t.boolean  "is_discounted"
    t.decimal  "discount_rate",                   precision: 10
    t.string   "status",              limit: 255
    t.date     "date_for_the_status"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "product_id"
    t.integer  "order_id"
  end

  add_index "order_lines", ["order_id"], name: "index_order_lines_on_order_id", using: :btree
  add_index "order_lines", ["product_id"], name: "index_order_lines_on_product_id", using: :btree

  create_table "orderlines", force: :cascade do |t|
    t.decimal  "quantity_in_kg",             precision: 10
    t.decimal  "total_amount",               precision: 10
    t.boolean  "is_discounted"
    t.decimal  "discount_rate",              precision: 10
    t.string   "status",         limit: 255
    t.date     "status_date"
    t.integer  "product_id"
    t.integer  "order_id"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "orderlines", ["order_id"], name: "fk_rails_0c0a821710", using: :btree
  add_index "orderlines", ["product_id"], name: "fk_rails_781a1ed986", using: :btree

  create_table "orders", force: :cascade do |t|
    t.date     "order_date"
    t.decimal  "total_price",                     precision: 10
    t.boolean  "is_discounted"
    t.decimal  "discount_rate",                   precision: 10
    t.string   "status",              limit: 255
    t.date     "date_for_the_status"
    t.date     "payment_due_date"
    t.integer  "customer_id"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  add_index "orders", ["customer_id"], name: "index_orders_on_customer_id", using: :btree

  create_table "payments", force: :cascade do |t|
    t.decimal  "amount_paid",              precision: 10
    t.string   "payment_type", limit: 255
    t.date     "date_paid"
    t.integer  "order_id"
    t.integer  "customer_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "payments", ["customer_id"], name: "fk_rails_e3a64e537e", using: :btree
  add_index "payments", ["order_id"], name: "fk_rails_6af949464b", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "threshold"
    t.decimal  "base_price",             precision: 6, scale: 2
    t.boolean  "status"
    t.integer  "quantity"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  create_table "stocks", force: :cascade do |t|
    t.date     "purchase_date"
    t.integer  "quantity"
    t.integer  "products_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_foreign_key "order_lines", "orders"
  add_foreign_key "order_lines", "products"
  add_foreign_key "orderlines", "orders"
  add_foreign_key "orderlines", "products"
  add_foreign_key "orders", "customers"
  add_foreign_key "payments", "customers"
  add_foreign_key "payments", "orders"
end
