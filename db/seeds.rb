# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
if Admin.count == 0
    Admin.create!({email: 'admin@example.com', password: '12341234', password_confirmation: '12341234', name: 'Manager'})
    puts "Admin created!"
end

if Customer.count == 0
    Customer.create!([
        {
        	name: "grace lim",
        	company: "milestone",
        	phone: "09171234567",
        	email: "grace@gmail.com",
        	home_address: "quezon city",
        	status: true,
        	is_loyal: true
        },
        {
        	name: "ysabel veron",
        	company: "mile",
        	phone: "09161234567",
        	email: "veron@email.com",
        	home_address: "antipolo",
        	status: true ,
        	is_loyal: false
        },
        {
        	name: "kimee lagura",
        	company: "stone",
        	phone: "091281234567",
        	email: "kim@hi.com",
        	home_address: "cebu",
        	status: true,
        	is_loyal: true
        }
    ])
end

if Product.count == 0
    Product.create!([
        {
        	name: "Nestea",
        	threshold: "4",
        	base_price: "30.0",
        	quantity: "3",
        	status: true,
        },
        {
        	name: "Sea Cucumber",
        	threshold: "10",
        	base_price: "30.0",
        	quantity: "5",
        	status: true,
        },
        {
        	name: "Land Cucumber",
        	threshold: "12",
        	base_price: "20.0",
        	quantity: "22",
        	status: true,
        }
    ])
end