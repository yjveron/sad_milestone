class CreateOrderlines < ActiveRecord::Migration
  def change
    create_table :orderlines do |t|
      t.decimal :quantity_in_kg
      t.decimal :total_amount
      t.boolean :is_discounted
      t.decimal :discount_rate
      t.string :status
      t.date :status_date
      t.references :product, foreign_key: true
      t.references :order, foreign_key: true

      t.timestamps null: false
    end
  end
end
