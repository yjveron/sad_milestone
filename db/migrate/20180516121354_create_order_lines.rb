class CreateOrderLines < ActiveRecord::Migration
  def change
    create_table :order_lines do |t|
      t.decimal :quantity_of_product
      t.boolean :is_discounted
      t.decimal :discount_rate
      t.string :status
      t.date :date_for_the_status

      t.timestamps null: false
    end
  end
end
