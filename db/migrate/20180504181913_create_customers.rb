class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.string :company
      t.string :phone
      t.string :email
      t.string :home_address
      t.boolean :status
      t.boolean :is_loyal

      t.timestamps null: false
    end
  end
end
