class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.date :order_date
      t.decimal :total_price
      t.boolean :is_discounted
      t.decimal :discount_rate
      t.string :status
      t.date :date_for_the_status
      t.date :payment_due_date
      t.references :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
