class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.decimal :amount_paid
      t.string :payment_type
      t.date :date_paid
      t.references :order, foreign_key: true
      t.references :customer, foreign_key: true
      t.timestamps null: false
    end
  end
end
