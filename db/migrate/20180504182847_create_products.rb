class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.integer :threshold
      t.decimal :base_price, precision: 6, scale: 2
      t.string :price_basis
      t.string :status
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
