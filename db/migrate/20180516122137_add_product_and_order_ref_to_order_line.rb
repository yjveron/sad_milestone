class AddProductAndOrderRefToOrderLine < ActiveRecord::Migration
  def self.up
    add_reference :order_lines, :product, index: true, foreign_key: true
    add_reference :order_lines, :order, index: true, foreign_key: true
  end
end
